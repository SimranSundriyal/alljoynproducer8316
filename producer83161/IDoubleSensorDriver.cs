﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace producer83161
{
    /// <summary>
    /// INterface for accessing sensor data.
    /// </summary>
    public interface IDoubleSensorDriver
    {
        Task<double> Read(string sensor);
        Task<string> ReadState(string sensor);
    }
}