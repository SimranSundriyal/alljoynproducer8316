﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.example.SafetyAlarm;
using System.Threading;
using Windows.Devices.AllJoyn;
using Windows.Foundation;
using System.Diagnostics;

namespace producer83161
{
    /// <summary>
    /// Class to provide async tasks required for implmenting program's logical components.
    /// </summary>
    class SafetyAlarmService : ISafetyAlarmService
    {
        readonly IDoubleSensorDriver SensorDriver;
        public Timer _timer;
        public static double _temperature;
        public static double _airQuality;
        public static int _light;
        public const int _sampleRateMilliseconds = 5000;
        public static double temperatureThreshold;
        public static double airQualityThreshold;
        public static string state;

        // Constructor for initialising thresholds.
      
        public SafetyAlarmService(IDoubleSensorDriver driver)
        {
            SensorDriver = driver;
            temperatureThreshold = 27.0;
            airQualityThreshold = 700.0;
            
           _timer = new Timer(tcb, null, TimeSpan.FromMilliseconds(_sampleRateMilliseconds), Timeout.InfiniteTimeSpan);
        }

        // Async function which will run every 5 sec to read Current sensor values.

        public async void tcb(object status)
        {

            _temperature = await SensorDriver.Read("temperature");
            _airQuality = await SensorDriver.Read("airQuality");
            _light = (short)await SensorDriver.Read("light");
            state = await SensorDriver.ReadState("state");
            _timer.Change(TimeSpan.FromMilliseconds(_sampleRateMilliseconds), Timeout.InfiniteTimeSpan);
        }

        /// <summary>
        /// Async Operation to be executed when user clicks on Stop Alert button.It Changes the State to normal. 
        /// </summary>
       

        public IAsyncOperation<SafetyAlarmStopAlertResult> StopAlertAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmStopAlertResult> task = new Task<SafetyAlarmStopAlertResult>(() =>
            {
                state = "normal";
                
                Debug.WriteLine("stop alert method called. stop alarm.");
                
                
                return SafetyAlarmStopAlertResult.CreateSuccessResult();
            });
            task.Start();
            return task.AsAsyncOperation();
        }

        /// <summary>
        ///  Async Operation to be executed when user clicks on Reser Alert button.It resets the thresholds to factory settings. 
        /// </summary>

        public IAsyncOperation<SafetyAlarmResetResult> ResetAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmResetResult> task = new Task<SafetyAlarmResetResult>(() =>
            {
                temperatureThreshold = 27;
                airQualityThreshold = 700;
                Debug.WriteLine("factory settings have been set");
                Debug.WriteLine("new temperature threshold= " + temperatureThreshold);
                Debug.WriteLine("new air quality threshold =" + airQualityThreshold);
                return SafetyAlarmResetResult.CreateSuccessResult();

            });
            task.Start();
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which returns the temperature threshold.
        /// </summary>


        public IAsyncOperation<SafetyAlarmGetTemperatureThresholdResult> GetTemperatureThresholdAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmGetTemperatureThresholdResult> task = new Task<SafetyAlarmGetTemperatureThresholdResult>(() =>
            {
                return SafetyAlarmGetTemperatureThresholdResult.CreateSuccessResult(temperatureThreshold);
            });
            task.Start();
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which sets the temperature threshold to the user provided values.
        /// </summary>


        public IAsyncOperation<SafetyAlarmSetTemperatureThresholdResult> SetTemperatureThresholdAsync(AllJoynMessageInfo info, double value)
        {
            Task<SafetyAlarmSetTemperatureThresholdResult> task = new Task<SafetyAlarmSetTemperatureThresholdResult>(() =>
            {
                temperatureThreshold = Convert.ToDouble(value);
                state = "normal";
                Debug.WriteLine("temperature threshold set");
                Debug.WriteLine(temperatureThreshold);
                return SafetyAlarmSetTemperatureThresholdResult.CreateSuccessResult();
            });
            task.Start();
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which returns the airQuality threshold.
        /// </summary>


        public IAsyncOperation<SafetyAlarmGetAirQualityThresholdResult> GetAirQualityThresholdAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmGetAirQualityThresholdResult> task = new Task<SafetyAlarmGetAirQualityThresholdResult>(() =>
            {
                return SafetyAlarmGetAirQualityThresholdResult.CreateSuccessResult(airQualityThreshold);
            });
            task.Start();
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which sets the airQuality threshold to the user provided values.
        /// </summary>


        public IAsyncOperation<SafetyAlarmSetAirQualityThresholdResult> SetAirQualityThresholdAsync(AllJoynMessageInfo info, double value)
        {
            Task<SafetyAlarmSetAirQualityThresholdResult> task = new Task<SafetyAlarmSetAirQualityThresholdResult>(() =>
            {
                airQualityThreshold = value;
                state = "normal";
                Debug.WriteLine("air quality threshold set");
                Debug.WriteLine(airQualityThreshold);
                return SafetyAlarmSetAirQualityThresholdResult.CreateSuccessResult();
            });
            task.Start();
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which returns the Current Temperature readings.
        /// </summary>


        public IAsyncOperation<SafetyAlarmGetCurrentTemperatureResult> GetCurrentTemperatureAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmGetCurrentTemperatureResult> task = new Task<SafetyAlarmGetCurrentTemperatureResult>(() => {
                return SafetyAlarmGetCurrentTemperatureResult.CreateSuccessResult(_temperature);
            });
            task.Start();
            Debug.WriteLine(" current temperature task called");
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which returns the Current Light readings.
        /// </summary>
  

        public IAsyncOperation<SafetyAlarmGetCurrentLightValueResult> GetCurrentLightValueAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmGetCurrentLightValueResult> task = new Task<SafetyAlarmGetCurrentLightValueResult>(() => {
                return SafetyAlarmGetCurrentLightValueResult.CreateSuccessResult((short)_light);
            });
            task.Start();
            Debug.WriteLine(" current air quality task called");
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which returns the Current AirQuality readings. 
        /// </summary>


        public IAsyncOperation<SafetyAlarmGetCurrentAirQualityResult> GetCurrentAirQualityAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmGetCurrentAirQualityResult> task = new Task<SafetyAlarmGetCurrentAirQualityResult>(() => {
                return SafetyAlarmGetCurrentAirQualityResult.CreateSuccessResult(_airQuality);
            });
            task.Start();
            Debug.WriteLine(" current light value task called");
            return task.AsAsyncOperation();
        }

        /// <summary>
        /// Async Operation which returns the Current State readings.
        /// </summary>


        public IAsyncOperation<SafetyAlarmGetStateResult> GetStateAsync(AllJoynMessageInfo info)
        {
            Task<SafetyAlarmGetStateResult> task = new Task<SafetyAlarmGetStateResult>(() => {
                return SafetyAlarmGetStateResult.CreateSuccessResult(state);
            });
            task.Start();
            Debug.WriteLine(" current state called");
            return task.AsAsyncOperation();
        }
    }
}
