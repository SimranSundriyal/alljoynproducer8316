﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.example.SafetyAlarm;
using GrovePi;
using GrovePi.Sensors;
using System.Diagnostics;

namespace producer83161
{
    /// <summary>
    /// Class for interfacing with Sensors.
    /// </summary>
    class SafetyAlarmDriver : IDoubleSensorDriver
    {
        readonly SafetyAlarmProducer _producer;
        int lightThreshold = 700;

        // Constructor to initialise the State to normal.
        public SafetyAlarmDriver(SafetyAlarmProducer producer)
        {
            _producer = producer;
            SafetyAlarmService.state = "normal";
        }

        // Task (async operation ) which returns values of different sensors.

        public async Task<double> Read(string sensor)
        {
            switch (sensor)
            {
                case "temperature":
                    // new temperature Device intitialised.
                    ITemperatureSensor temp = DeviceFactory.Build.TemperatureSensor(GrovePi.Pin.AnalogPin0, TemperatureSensorModel.OnePointTwo);
                    SafetyAlarmService._temperature = temp.TemperatureInCelcius();
                    
                    // Notify of change in Temperature 
                    _producer.EmitCurrentTemperatureChanged();

                    // Change state in case temperature crosses threshold
                    if (SafetyAlarmService._temperature >= SafetyAlarmService.temperatureThreshold)
                    {
                                              
                            SafetyAlarmService.state = "alert";

                            // Signal raised in case of alert state.
                            
                            _producer.Signals.AlertRaised(true, false);
                            Debug.WriteLine("alert raised signal sent");
           
                    }
                   
                   
                    Debug.WriteLine("Temperature = " + SafetyAlarmService._temperature);
                    return SafetyAlarmService._temperature;

                case "airQuality":

                    // new airquality Device intitialised.
                    // Airquality sensor hasn't been added to nuget package, hence using temperatureSesnor for temperary replacement.

                    ITemperatureSensor airQual = DeviceFactory.Build.TemperatureSensor(GrovePi.Pin.AnalogPin1, TemperatureSensorModel.OnePointTwo);
                    SafetyAlarmService._airQuality = airQual.TemperatureInCelcius();

                    // Notify of change in airQuality. 

                    _producer.EmitCurrentAirQualityChanged();

                    // Change state in case temperature crosses threshold

                    if (SafetyAlarmService._airQuality >= SafetyAlarmService.airQualityThreshold)
                    {
                        // Signal raised in case of alert state.

                        _producer.Signals.AlertRaised(false, true);
                        SafetyAlarmService.state = "alert";

                        Debug.WriteLine("alert raised  signal sent");
                    }
                   
                    
                    Debug.WriteLine("Air Quality = " + SafetyAlarmService._airQuality);
                    return SafetyAlarmService._airQuality;

                case "light":

                    // new LightSensor Device intitialised.

                    ILightSensor lit = DeviceFactory.Build.LightSensor(GrovePi.Pin.AnalogPin2);
                    SafetyAlarmService._light = lit.SensorValue();

                    // Notify of change in airQuality. 

                    _producer.EmitCurrentLightValueChanged();

                    // Change state in case light crosses threshold

                    if (SafetyAlarmService._light >= lightThreshold)
                    {

                        // Signal raised in case of alert state.

                        _producer.Signals.LightStateChanged((short)SafetyAlarmService._light);
                        Debug.WriteLine("light state changed signal sent");
                     }
                    Debug.WriteLine("Light Value= " + SafetyAlarmService._light);
                    return SafetyAlarmService._light;
                


                default:
                    Debug.WriteLine("No sensor detected");
                    return 0;

            }
        }

        /// <summary>
        /// Task (async operation ) which returns current state of sensors.
        /// </summary>

        public async Task<string> ReadState(string sensor)
        {
            switch (sensor)
            {
                case "state":

                    _producer.EmitStateChanged();
                    return SafetyAlarmService.state;
                default:
                    return "null";
            }

            }

    }
}
