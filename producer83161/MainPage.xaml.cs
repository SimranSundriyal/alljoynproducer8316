﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using com.example.SafetyAlarm;
using Windows.Devices.AllJoyn;
using System.Diagnostics;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace producer83161
{
    /// <summary>
    /// Class for interacting with mainPAGE.xaml
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Driver object created for accessing Sensor Data and attaching it to producer.
        /// Producer object created for attaching it to the the bus.
        /// </summary>
        private SafetyAlarmDriver _driver;
        private SafetyAlarmProducer _producer;
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
            this.Unloaded += MainPage_Unloaded;
        }

        /// <summary>
        /// On main page being loaded a new bus is created , Anonymous  authentication is done, then producer and drivers are attached subsequently. 
        /// </summary>
      
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            AllJoynBusAttachment bus = new AllJoynBusAttachment();
            bus.AuthenticationMechanisms.Add(AllJoynAuthenticationMechanism.SrpAnonymous);
            _producer = new SafetyAlarmProducer(bus);
            _driver = new SafetyAlarmDriver(_producer);
            _producer.Service = new SafetyAlarmService(_driver);
            _producer.Start();
            Debug.WriteLine("Bus started");

        }
        private void MainPage_Unloaded(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }


    }
}
