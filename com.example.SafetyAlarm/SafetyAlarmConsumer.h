//-----------------------------------------------------------------------------
// <auto-generated> 
//   This code was generated by a tool. 
// 
//   Changes to this file may cause incorrect behavior and will be lost if  
//   the code is regenerated.
//
//   Tool: AllJoynCodeGenerator.exe
//
//   This tool is located in the Windows 10 SDK and the Windows 10 AllJoyn 
//   Visual Studio Extension in the Visual Studio Gallery.  
//
//   The generated code should be packaged in a Windows 10 C++/CX Runtime  
//   Component which can be consumed in any UWP-supported language using 
//   APIs that are available in Windows.Devices.AllJoyn.
//
//   Using AllJoynCodeGenerator - Invoke the following command with a valid 
//   Introspection XML file and a writable output directory:
//     AllJoynCodeGenerator -i <INPUT XML FILE> -o <OUTPUT DIRECTORY>
// </auto-generated>
//-----------------------------------------------------------------------------
#pragma once

namespace com { namespace example { namespace SafetyAlarm {

public interface class ISafetyAlarmConsumer
{
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionLostEventArgs^>^ SessionLost;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionMemberAddedEventArgs^>^ SessionMemberAdded;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionMemberRemovedEventArgs^>^ SessionMemberRemoved;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ CurrentTemperatureChanged;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ CurrentLightValueChanged;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ CurrentAirQualityChanged;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ StateChanged;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ TemperatureThresholdChanged;
    event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ AirQualityThresholdChanged;
};

public ref class SafetyAlarmConsumer sealed  : [Windows::Foundation::Metadata::Default] ISafetyAlarmConsumer
{
public:
    SafetyAlarmConsumer(Windows::Devices::AllJoyn::AllJoynBusAttachment^ busAttachment);
    virtual ~SafetyAlarmConsumer();

    // Join the AllJoyn session specified by sessionName.
    //
    // This will usually be called after the unique name of a producer has been reported
    // in the Added callback on the Watcher.
    static Windows::Foundation::IAsyncOperation<SafetyAlarmJoinSessionResult^>^ JoinSessionAsync(_In_ Windows::Devices::AllJoyn::AllJoynServiceInfo^ serviceInfo, _Inout_ SafetyAlarmWatcher^ watcher);

    // Call the StopAlert method
    Windows::Foundation::IAsyncOperation<SafetyAlarmStopAlertResult^>^ StopAlertAsync();
    // Call the Reset method
    Windows::Foundation::IAsyncOperation<SafetyAlarmResetResult^>^ ResetAsync();

    // This event fires whenever the value of CurrentTemperature changes.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ CurrentTemperatureChanged 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ handler) 
        { 
            return _CurrentTemperatureChanged += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Platform::Object^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _CurrentTemperatureChanged -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Platform::Object^ args) 
        { 
            _CurrentTemperatureChanged(sender, args);
        } 
    }
    
    // Get the value of the CurrentTemperature property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmGetCurrentTemperatureResult^>^ GetCurrentTemperatureAsync();

    // This event fires whenever the value of CurrentLightValue changes.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ CurrentLightValueChanged 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ handler) 
        { 
            return _CurrentLightValueChanged += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Platform::Object^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _CurrentLightValueChanged -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Platform::Object^ args) 
        { 
            _CurrentLightValueChanged(sender, args);
        } 
    }
    
    // Get the value of the CurrentLightValue property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmGetCurrentLightValueResult^>^ GetCurrentLightValueAsync();

    // This event fires whenever the value of CurrentAirQuality changes.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ CurrentAirQualityChanged 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ handler) 
        { 
            return _CurrentAirQualityChanged += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Platform::Object^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _CurrentAirQualityChanged -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Platform::Object^ args) 
        { 
            _CurrentAirQualityChanged(sender, args);
        } 
    }
    
    // Get the value of the CurrentAirQuality property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmGetCurrentAirQualityResult^>^ GetCurrentAirQualityAsync();

    // This event fires whenever the value of State changes.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ StateChanged 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ handler) 
        { 
            return _StateChanged += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Platform::Object^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _StateChanged -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Platform::Object^ args) 
        { 
            _StateChanged(sender, args);
        } 
    }
    
    // Get the value of the State property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmGetStateResult^>^ GetStateAsync();

    // This event fires whenever the value of TemperatureThreshold changes.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ TemperatureThresholdChanged 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ handler) 
        { 
            return _TemperatureThresholdChanged += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Platform::Object^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _TemperatureThresholdChanged -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Platform::Object^ args) 
        { 
            _TemperatureThresholdChanged(sender, args);
        } 
    }
    
    // Get the value of the TemperatureThreshold property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmGetTemperatureThresholdResult^>^ GetTemperatureThresholdAsync();

    // Set the value of the TemperatureThreshold property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmSetTemperatureThresholdResult^>^ SetTemperatureThresholdAsync(_In_ double value);
    // This event fires whenever the value of AirQualityThreshold changes.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ AirQualityThresholdChanged 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Platform::Object^>^ handler) 
        { 
            return _AirQualityThresholdChanged += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Platform::Object^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _AirQualityThresholdChanged -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Platform::Object^ args) 
        { 
            _AirQualityThresholdChanged(sender, args);
        } 
    }
    
    // Get the value of the AirQualityThreshold property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmGetAirQualityThresholdResult^>^ GetAirQualityThresholdAsync();

    // Set the value of the AirQualityThreshold property.
    Windows::Foundation::IAsyncOperation<SafetyAlarmSetAirQualityThresholdResult^>^ SetAirQualityThresholdAsync(_In_ double value);

    // Used to send signals or register functions to handle received signals.
    property SafetyAlarmSignals^ Signals
    {
        SafetyAlarmSignals^ get() { return m_signals; }
    }

    // This event will fire whenever the consumer loses the session that it is a member of.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionLostEventArgs^>^ SessionLost 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionLostEventArgs^>^ handler) 
        { 
            return _SessionLost += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Windows::Devices::AllJoyn::AllJoynSessionLostEventArgs^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _SessionLost -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Windows::Devices::AllJoyn::AllJoynSessionLostEventArgs^ args) 
        { 
            _SessionLost(sender, args);
        } 
    }

    // This event will fire whenever a member joins the session.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionMemberAddedEventArgs^>^ SessionMemberAdded 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionMemberAddedEventArgs^>^ handler) 
        { 
            return _SessionMemberAdded += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Windows::Devices::AllJoyn::AllJoynSessionMemberAddedEventArgs^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _SessionMemberAdded -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Windows::Devices::AllJoyn::AllJoynSessionMemberAddedEventArgs^ args) 
        { 
            _SessionMemberAdded(sender, args);
        } 
    }

    // This event will fire whenever a member leaves the session.
    virtual event Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionMemberRemovedEventArgs^>^ SessionMemberRemoved 
    { 
        Windows::Foundation::EventRegistrationToken add(Windows::Foundation::TypedEventHandler<SafetyAlarmConsumer^, Windows::Devices::AllJoyn::AllJoynSessionMemberRemovedEventArgs^>^ handler) 
        { 
            return _SessionMemberRemoved += ref new Windows::Foundation::EventHandler<Platform::Object^>
            ([handler](Platform::Object^ sender, Platform::Object^ args)
            {
                handler->Invoke(safe_cast<SafetyAlarmConsumer^>(sender), safe_cast<Windows::Devices::AllJoyn::AllJoynSessionMemberRemovedEventArgs^>(args));
            }, Platform::CallbackContext::Same);
        } 
        void remove(Windows::Foundation::EventRegistrationToken token) 
        { 
            _SessionMemberRemoved -= token; 
        } 
    internal: 
        void raise(SafetyAlarmConsumer^ sender, Windows::Devices::AllJoyn::AllJoynSessionMemberRemovedEventArgs^ args) 
        { 
            _SessionMemberRemoved(sender, args);
        } 
    }

internal:
    // Consumers do not support property get.
    QStatus OnPropertyGet(_In_ PCSTR interfaceName, _In_ PCSTR propertyName, _Inout_ alljoyn_msgarg val) 
    { 
        UNREFERENCED_PARAMETER(interfaceName); UNREFERENCED_PARAMETER(propertyName); UNREFERENCED_PARAMETER(val); 
        return ER_NOT_IMPLEMENTED; 
    }

    // Consumers do not support property set.
    QStatus OnPropertySet(_In_ PCSTR interfaceName, _In_ PCSTR propertyName, _In_ alljoyn_msgarg val) 
    { 
        UNREFERENCED_PARAMETER(interfaceName); UNREFERENCED_PARAMETER(propertyName); UNREFERENCED_PARAMETER(val);
        return ER_NOT_IMPLEMENTED;
    }

    void OnSessionLost(_In_ alljoyn_sessionid sessionId, _In_ alljoyn_sessionlostreason reason);
    void OnSessionMemberAdded(_In_ alljoyn_sessionid sessionId, _In_ PCSTR uniqueName);
    void OnSessionMemberRemoved(_In_ alljoyn_sessionid sessionId, _In_ PCSTR uniqueName);

    void OnPropertyChanged(_In_ alljoyn_proxybusobject obj, _In_ PCSTR interfaceName, _In_ const alljoyn_msgarg changed, _In_ const alljoyn_msgarg invalidated);

    property Platform::String^ ServiceObjectPath
    {
        Platform::String^ get() { return m_ServiceObjectPath; }
        void set(Platform::String^ value) { m_ServiceObjectPath = value; }
    }

    property alljoyn_proxybusobject ProxyBusObject
    {
        alljoyn_proxybusobject get() { return m_proxyBusObject; }
        void set(alljoyn_proxybusobject value) { m_proxyBusObject = value; }
    }

    property alljoyn_busobject BusObject
    {
        alljoyn_busobject get() { return m_busObject; }
        void set(alljoyn_busobject value) { m_busObject = value; }
    }
    
    property alljoyn_sessionlistener SessionListener
    {
        alljoyn_sessionlistener get() { return m_sessionListener; }
        void set(alljoyn_sessionlistener value) { m_sessionListener = value; }
    }

    property alljoyn_sessionid SessionId
    {
        alljoyn_sessionid get() { return m_sessionId; }
    }
    
private:
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _SessionLost;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _SessionMemberAdded;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _SessionMemberRemoved;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _CurrentTemperatureChanged;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _CurrentLightValueChanged;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _CurrentAirQualityChanged;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _StateChanged;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _TemperatureThresholdChanged;
    virtual event Windows::Foundation::EventHandler<Platform::Object^>^ _AirQualityThresholdChanged;

    int32 JoinSession(_In_ Windows::Devices::AllJoyn::AllJoynServiceInfo^ serviceInfo);

    // Register a callback function to handle incoming signals.
    QStatus AddSignalHandler(_In_ alljoyn_busattachment busAttachment, _In_ alljoyn_interfacedescription interfaceDescription, _In_ PCSTR methodName, _In_ alljoyn_messagereceiver_signalhandler_ptr handler);

    static void CallAlertRaisedSignalHandler(_In_ const alljoyn_interfacedescription_member* member, _In_ alljoyn_message message);
    static void CallLightStateChangedSignalHandler(_In_ const alljoyn_interfacedescription_member* member, _In_ alljoyn_message message);
    
    Windows::Devices::AllJoyn::AllJoynBusAttachment^ m_busAttachment;
    SafetyAlarmSignals^ m_signals;
    Platform::String^ m_ServiceObjectPath;

    alljoyn_proxybusobject m_proxyBusObject;
    alljoyn_busobject m_busObject;
    alljoyn_sessionlistener m_sessionListener;
    alljoyn_sessionid m_sessionId;
    alljoyn_busattachment m_nativeBusAttachment;

    // Used to pass a pointer to this class to callbacks
    Platform::WeakReference* m_weak;

    // This map is required because we need a way to pass the consumer to the signal
    // handlers, but the current AllJoyn C API does not allow passing a context to these
    // callbacks.
    static std::map<alljoyn_interfacedescription, Platform::WeakReference*> SourceInterfaces;
};

} } } 
